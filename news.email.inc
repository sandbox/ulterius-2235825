<?php
function form_emailsettings_news() {
    global $user;
    $form['news_header'] = array('#type' => 'text_format', '#default_value' => variable_get('news_header', ''), '#title' => t('Edit mail header'), '#description' => t('Header part of the news letter'), '#cols' => 60, '#rows' => 10,);
    $form['news_signature'] = array('#type' => 'text_format', '#default_value' => variable_get('news_signature', ''), '#title' => t('Edit mail footer'), '#description' => t('Footer part of the news letter'), '#cols' => 60, '#rows' => 10,);
    $form['news_subscribe'] = array('#type' => 'text_format', '#default_value' => variable_get('news_subscribe', ''), '#title' => t('Subscribe text'), '#description' => t('Subscribe email confirmation'), '#cols' => 60, '#rows' => 10,);
    $form['news_welcome'] = array('#type' => 'text_format', '#default_value' => variable_get('news_welcome', ''), '#title' => t('Welcome subscribe text'), '#description' => t('Mail welcome text'), '#cols' => 60, '#rows' => 10,);
    $form['news_emails_receive'] = array('#type' => 'textfield', '#title' => t('Email address used for news'), '#size' => 60, '#default_value' => variable_get('news_emails_receive', $user->email),);
    $form['submit'] = array('#type' => 'submit', '#value' => t('Submit'));
    return $form;
}
function form_emailsettings_news_submit($form, &$form_state) {
    variable_set('news_signature', $form_state['values']['news_signature']['value']);
    variable_set('news_header', $form_state['values']['news_header']['value']);
    variable_set('news_subscribe', $form_state['values']['news_subscribe']['value']);
    variable_set('news_welcome', $form_state['values']['news_welcome']['value']);
    variable_set('news_emails_receive', $form_state['values']['news_emails_receive']);
    drupal_set_message(t('Settings stored'));
}
function news_email($form, &$form_state, $node) {
    $form['node'] = array('#type' => 'value', '#value' => $node);
    $content = node_load($node, NULL, TRUE);
    $msg = t('Send this news?: ') . $content->title;
    $form['submit'] = array('#prefix' => $msg, '#type' => 'submit', '#value' => t('Are you sure?'));
    $form['cancel'] = array('#type' => 'submit', '#value' => t('Cancel'));
    return $form;
}
function news_email_submit($form, &$form_state) {
    global $base_url;
    $nid = $form_state['values']['node'];
    if ($form_state['clicked_button']['#id'] == 'edit-cancel') {
        drupal_goto('/news/');
    } else {
        $module = 'informatie';
        $key = microtime();
        $language = language_default();
        $params = array();
        $send = FALSE;
        global $user;
        global $base_url;
        $rows = array();
        $node = node_load($nid);
        $from = variable_get('news_emails_receive', '');
        $header = array();
        //header part
        $options = array('html' => TRUE, 'attributes' => array('class' => 'titlelink', 'title' => l('Read this newsletter on the website')));
        $title = '<center>' . l($node->title, $base_url . '/node/' . $nid, $options) . '</center>';
        $rows[] = array(array('data' => variable_get('news_header', '') . $title, 'class' => 'header'));
        //body part
        //		This method is too noisy.
        //		$body = drupal_render(node_view($node, 'full'));
        //		$body=drupal_render(node_view(node_load($node)),'full');
        $body = field_get_items('node', $node, 'body');
        $bodytext = field_view_value('node', $node, 'body', $body[0], 'full');
        $rows[] = array(array('data' => $bodytext, 'class' => 'body'));
        $result = db_query("SELECT email,code FROM {newsemail_current");
        foreach ($result as $row) {
            $unsubscribe = "<p>" . l(t('Click here to unsubscribe'), $base_url . "/news/unsubscribe/" . $row->code) . "</p>";
            //footer part
            $rows[] = array(array('data' => variable_get('news_signature', '') . $unsubscribe, 'class' => 'footer'));
            $body = theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('class' => array('news_table'))));
            $send = FALSE;
            $message = drupal_mail($module, $key, $row->email, $language, $params, $from, $send);
            $message['subject'] = "[" . variable_get('site_name', '') . "] " . $node->title;
            $message['body'] = $body;
            // Retrieve the responsible implementation for this message.
            $system = drupal_mail_system($module, $key);
            // Format the message body.
            $message = $system->format($message);
            // Send e-mail.
            $message['result'] = $system->mail($message);
            $emails.= $row->email . " ";
        }
        $entry['time'] = time();
        $entry['node'] = $nid;
        $entry['subject'] = $node->title;
        $entry['emails'] = $emails;
        try {
            $id = db_insert('newsemail_send')->fields($entry)->execute();
        }
        catch(Exception $e) {
            drupal_set_message(t('db_insert failed. Message = %message, query= %query', array('%message' => $e->getMessage(), '%query' => $e->query_string)), 'error');
        }
        drupal_set_message($node->title . t("emails has been send"));
        drupal_goto('/news');
        return;
    }
}
function news_subscribe_block_form() {
    $form = array();
    $form['email'] = array('#type' => 'textfield', '#title' => t('Latest news email subscription'), '#description' => t('Email address'), '#size' => 20,);
    $form['submit'] = array('#type' => 'submit', '#value' => t('Send'));
    return $form;
}
function news_subscribe_block_form_submit($form, &$form_state) {
    global $user;
    global $base_url;
    $email = trim($form_state['values']['email']);
    if (valid_email_address($email)) {
        $row = db_query("SELECT email FROM {newsemail_current} where email=:email;", array(':email' => $email))->fetchObject();
        if ($row) {
            drupal_set_message($email . t(' allready exists!'), 'error');
        } else {
            $from = variable_get('news_emails_receive', '');
            $time = time();
            $code = md5($email . $time);
            $entry['time'] = $time;
            $entry['email'] = $email;
            $entry['code'] = $code;
            try {
                $id = db_insert('newsemail_new')->fields($entry)->execute();
            }
            catch(Exception $e) {
                drupal_set_message(t('db_insert failed. Message = %message, query= %query', array('%message' => $e->getMessage(), '%query' => $e->query_string)), 'error');
                return;
            }
            $module = 'news';
            $key = microtime();
            $language = language_default();
            $params = array();
            $send = FALSE;
            $rows = array();
            $header = array();
            //body part
            $rows[] = array(array('data' => variable_get('news_subscribe', '') . l(t('Click here to subscribe'), $base_url . "/news/subscribe/" . $code), 'class' => 'body'));
            //footer part
            $rows[] = array(array('data' => variable_get('news_signature', '') . $unsubscribe, 'class' => 'footer'));
            $body = theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('class' => array('news_table'))));
            $send = FALSE;
            $message = drupal_mail($module, $key, $email, $language, $params, $from, $send);
            $message['subject'] = t("Subscription confirmation") . " " . variable_get('site_name', '');
            $message['body'] = $body;
            // Retrieve the responsible implementation for this message.
            $system = drupal_mail_system($module, $key);
            // Format the message body.
            $message = $system->format($message);
            // Send e-mail.
            $message['result'] = $system->mail($message);
            drupal_set_message(t('Confirmation mail is send to ') . $email . t(' please click on the link in the mail to confirm your subscription.'));
        }
    } else {
        drupal_set_message(t('Please enter a valid email address'), 'error');
    }
    return;
}
function news_subscribe($code) {
    global $base_url;
    $row = db_query("SELECT email FROM {newsemail_new} where code=:code;", array(':code' => $code))->fetchObject();
    if ($row->email) {
        $email = $row->email;
        $entry['time'] = time();
        $entry['email'] = $email;
        $entry['code'] = $code;
        try {
            $id = db_insert('newsemail_current')->fields($entry)->execute();
        }
        catch(Exception $e) {
            drupal_set_message(t('db_insert failed. Message = %message, query= %query', array('%message' => $e->getMessage(), '%query' => $e->query_string)), 'error');
            return;
        }
        //Remove from new subscribed table
        try {
            db_delete('newsemail_new')->condition('code', $code)->execute();
        }
        catch(Exception $e) {
            drupal_set_message(t('db_delete failed. Message = %message, query= %query', array('%message' => $e->getMessage(), '%query' => $e->query_string)), 'error');
            return;
        }
        $out = t('You have been added to the email news subscription list of ') . variable_get('site_name', '');
        $from = variable_get('news_emails_receive', '');
        $module = 'news';
        $key = microtime();
        $language = language_default();
        $params = array();
        $send = FALSE;
        $rows = array();
        $header = array();
        //body part
        $bodytext = variable_get('news_welcome', '');
        $bodytext.= "<p>";
        $bodytext.= l(t('Click here to unsubscribe'), $base_url . "/news/unsubscribe/" . $code);
        $rows[] = array(array('data' => $bodytext, 'class' => 'body'));
        //footer part
        $rows[] = array(array('data' => variable_get('news_signature', '') . $unsubscribe, 'class' => 'footer'));
        $body = theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('class' => array('news_table'))));
        $message = drupal_mail($module, $key, $email, $language, $params, $from, $send);
        $message['subject'] = t("Subscription confirmation") . " " . variable_get('site_name', '');
        $message['body'] = $body;
        // Retrieve the responsible implementation for this message.
        $system = drupal_mail_system($module, $key);
        // Format the message body.
        $message = $system->format($message);
        // Send e-mail.
        $message['result'] = $system->mail($message);
    } else {
        $out = t('Your email address is not found in de list at ') . variable_get('site_name', '');
    }
    drupal_set_message($out);
    drupal_goto('/news');
    return;
}
function news_unsubscribe($code) {
    $row = db_query("SELECT email FROM {newsemail_current} where code=:code;", array(':code' => $code))->fetchObject();
    if ($row->email) {
        $email = $row->email;
        $time = time();
        $entry['time'] = time();
        $entry['email'] = $email;
        $entry['code'] = $code;
        try {
            $id = db_insert('newsemail_old')->fields($entry)->execute();
        }
        catch(Exception $e) {
            drupal_set_message(t('db_insert failed. Message = %message, query= %query', array('%message' => $e->getMessage(), '%query' => $e->query_string)), 'error');
            return;
        }
        try {
            db_delete('newsemail_current')->condition('code', $code)->execute();
        }
        catch(Exception $e) {
            drupal_set_message(t('db_delete failed. Message = %message, query= %query', array('%message' => $e->getMessage(), '%query' => $e->query_string)), 'error');
            return;
        }
        $out = t('You have been removed from the email news subscription list of ') . variable_get('site_name', '');
    } else {
        $out = t('Your email address is not found in de list at ') . variable_get('site_name', '');
    }
    drupal_set_message($out);
    drupal_goto('/news');
    return;
}
function newsemail_send($node) {
    $tablerows = array();
    $result = db_query("SELECT * FROM {newsemail_send} where node=:node", array(':node' => $node));
    if (!$result) {
        drupal_set_message(t('Error getting data from newsemail_send'), 'error');
    }
    foreach ($result as $row) {
        $tablerow = array(format_date($row->time, 'full'), $row->subject, $row->emails);
        $tablerows[] = $tablerow;
    }
    $header = array(t('Time'), t('Title'), t('Email addresses'));
    $out.= theme('table', array('header' => $header, 'rows' => $tablerows, 'attributes' => array('class' => array('news_table'))));
    $out.= l(t('Go back'), 'news');
    return ($out);
}
function newsemail_admin() {
    module_load_include('inc', 'news', 'news.email');
    $out.= l(t("List all subscribed email addresses"), 'news/admin/list');
    $out.= "<p><hr><p>";
    $out.= drupal_render(drupal_get_form('news_add_emailaddress_form'));
    $out.= "<p><hr><p>";
    $out.= drupal_render(drupal_get_form('news_delete_emailaddress_form'));
    return ($out);
}
function news_add_emailaddress_form() {
    $form = array();
    $form['email'] = array('#type' => 'textfield', '#title' => t('Add email address'), '#size' => 20,);
    $form['submit'] = array('#type' => 'submit', '#value' => t('Add'));
    return $form;
}
function news_add_emailaddress_form_submit($form, &$form_state) {
    $email = trim($form_state['values']['email']);
    $row = db_query("SELECT email FROM {newsemail_current} where email=:email", array(':email' => $email))->fetchObject();
    if ($row) {
        drupal_set_message($email . t(' allready exists!'), 'error');
    } else {
        $entry['time'] = time();
        $entry['email'] = $email;
        $entry['code'] = md5($email . $time);;
        try {
            $id = db_insert('newsemail_current')->fields($entry)->execute();
        }
        catch(Exception $e) {
            drupal_set_message(t('db_insert failed. Message = %message, query= %query', array('%message' => $e->getMessage(), '%query' => $e->query_string)), 'error');
            return;
        }
    }
    return;
}
function newsemail_list_current() {
    $tablerows = array();
    $result = db_query("SELECT time,email FROM {newsemail_current} order by 1 DESC");
    foreach ($result as $row) {
        $tablerow = array(format_date($row->time, 'full'), $row->email);
        $tablerows[] = $tablerow;
    }
    $header = array(t('Time'), t('Email address'));
    $out.= theme('table', array('header' => $header, 'rows' => $tablerows, 'attributes' => array('class' => array('news_table'))));
    $out.= l(t('Go back'), 'news/admin');
    return ($out);
}
function news_delete_emailaddress_form() {
    $email_array = array();
    $email_array[''] = '';
    $result = db_query("SELECT time,email,code FROM {newsemail_current order by 1 DESC");
    foreach ($result as $row) {
        $email_array[$row->code] = $row->email;
    }
    $form['code'] = array('#type' => 'select', '#options' => $email_array, '#title' => t('Select the email address to delete'),);
    $form['submit'] = array('#type' => 'submit', '#value' => t('Delete'));
    return $form;
}
function news_delete_emailaddress_form_submit($form, &$form_state) {
    $code = $form_state['values']['code'];
    $row = db_query("SELECT email FROM {newsemail_current} where code=:code", array(':code' => $code))->fetchObject();
    if ($row->email) {
        $time = time();
        $entry['time'] = time();
        $entry['email'] = $row->email;
        $entry['code'] = $code;
        try {
            $id = db_insert('newsemail_old')->fields($entry)->execute();
        }
        catch(Exception $e) {
            drupal_set_message(t('db_insert failed. Message = %message, query= %query', array('%message' => $e->getMessage(), '%query' => $e->query_string)), 'error');
            return;
        }
        try {
            db_delete('newsemail_current')->condition('code', $code)->execute();
        }
        catch(Exception $e) {
            drupal_set_message(t('db_delete failed. Message = %message, query= %query', array('%message' => $e->getMessage(), '%query' => $e->query_string)), 'error');
            return;
        }
    } else {
        $out = t('The email address is not found in de list');
    }
}
