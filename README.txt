Readme file for the News letter module for Drupal
---------------------------------------------

This News module is based  on the core Blog.  News (Blog) items can send to the email list  in html format with the mimemail module.

Installation:
  Installation is like with all normal drupal modules:
  extract the 'news' folder from the tar ball to the
  modules directory from your website (typically sites/all/modules).
  Edit and copy de mail.css to your  theme directory for the Mimemail module.
